<?php
$data['visit_id'] = $visit_id;

/*Procedures*/
$rs2 = $this->nurse_model->get_visit_procedure_charges($visit_id);
$visit_procudures_display = "
<table align='center' class='table table-striped table-hover table-condensed'>
	<tr>
		
		<th>Procedure</th>
		<th>Units</th>
		<th>Unit Cost</th>
		<th>Total</th>
	</tr>		
";                     
		$total= 0;  
		if(count($rs2) >0){
			foreach ($rs2 as $key1):
				$v_procedure_id = $key1->visit_charge_id;
				$procedure_id = $key1->service_charge_id;
				$visit_charge_amount = $key1->visit_charge_amount;
				$units = $key1->visit_charge_units;
				$procedure_name = $key1->service_charge_name;
				$service_id = $key1->service_id;
			
				$total= $total +($units * $visit_charge_amount);
				
				$visit_procudures_display .= "
						<tr> 
							<td align='center'>".$procedure_name."</td>
							<td align='center'>".$units."</td>
							<td align='center'>".number_format($visit_charge_amount)."</td>
							<td align='center'>".number_format($units * $visit_charge_amount)."</td>
							</td>
						</tr>	
				";
				endforeach;

		}
$visit_procudures_display .= "
<tr bgcolor='#D9EDF7'>
<th colspan='3'>Grand Total: </th>
<th><div id='grand_total'>".number_format($total)."</div></th>
</tr>
 </table>
";



/*Vaccines*/
$visit__rs1 = $this->nurse_model->get_visit_vaccine_charges($visit_id);

$visit_vaccines_display = "
	<table align='center' class='table table-striped table-hover table-condensed'>
	<tr>
		<th>#</th>
		<th>Vaccine</th>
		<th>Units</th>
		<th>Unit Cost</th>
		<th>Total</th>
	</tr>
";                      $total= 0;  
						$count = 0;
						foreach ($visit__rs1 as $key1) :
							$v_vaccine_id = $key1->visit_charge_id;
							$vaccine_id = $key1->service_charge_id;
							$visit_charge_amount = $key1->visit_charge_amount;
							$units = $key1->visit_charge_units;
	 						$vaccine_name = $key1->service_charge_name;
							$service_id = $key1->service_id;
						
							$total= $total +($units * $visit_charge_amount);
							$count++;
							
								$visit_vaccines_display .= "
										<tr> 
											<td>".$count."</td>
								 			<td align='center'>".$vaccine_name."</td>
											<td align='center'>".$units."</td>
											<td align='center'>".number_format($visit_charge_amount)."</td>
											<td align='center'>".number_format($units * $visit_charge_amount)."</div></td>
										</tr>	
								";	
								
			endforeach;
$visit_vaccines_display .= "
<tr bgcolor='#D9EDF7'>
	<th colspan='4'>Grand Total: </th>
	<th><div id='grand_total'>".number_format($total)."</div></th>
</tr>
 </table>
";
?>
<div class="row">
	<div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <section class="panel panel-featured panel-featured-info">
                    <header class="panel-heading">
                        <h2 class="panel-title">Vitals</h2>
                    </header>
                    <div class="panel-body">
                        <!-- vitals from java script -->
                        <?php echo $this->load->view("nurse/show_previous_vitals", $data, TRUE);?>
                        <!-- end of vitals data -->
                    </div>
                 </section>
            </div>
        </div>
    </div>
    <div class="col-md-12">
		<section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Procedures</h2>
            </header>
            <div class="panel-body">
                <!-- visit Procedures from java script -->
                <?php echo $visit_procudures_display;?>
                <!-- end of visit procedures -->
            </div>
         </section>
    </div>
    <div class="col-md-12">
    	<section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Vaccines</h2>
            </header>
            <div class="panel-body">
                <!-- visit Procedures from java script -->
                <?php echo $visit_vaccines_display;?>
                <!-- end of visit procedures -->
            </div>
         </section>
    </div>
</div>
