<?php
		
$result = '';
$year_search = $this->session->userdata('moh_year');
$month_search = $this->session->userdata('moh_month');
if(!empty($year_search) AND !empty($month_search))
{
	$date = $year_search.'-'.$month_search.'-01';
	$end = $year_search.'-'.$month_search.'-' . date('t', strtotime($date));
}
else if(!empty($year_search) AND empty($month_search))
{
	// $date = $year_search.'-01-01';
	// $end = $year_search.'-12-' . date('t', strtotime($date));
	$date = $year_search.'-'.date('m-').'01';
	$end = $year_search.'-'.date('m').'-' . date('t', strtotime($date)); //get end date of month
}
else if(empty($year_search) AND !empty($month_search))
{
	// $date = $year_search.'-01-01';
	// $end = $year_search.'-12-' . date('t', strtotime($date));
	$date = date('Y-').$month_search.'-01';
	$end = date('Y-').$month_search.'-' . date('t', strtotime($date)); //get end date of month
}
else
{
	$date = date('Y-m-').'01';
	$end = date('Y').'-'.date('m').'-' . date('t', strtotime($date)); //get end date of month
}
$title_content = 'Showing diagnosis for '.date('M Y', strtotime($date));

$items_list = '';
while(strtotime($date) <= strtotime($end)) {
	$day_num = date('d', strtotime($date));
	$day_name = date('l', strtotime($date));
	$date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
	$items_list .= "<th>$day_num</th>";
}
  
 

//if users exist display them
if ($query->num_rows() > 0)
{
	$count = 0;
	
	$result .= 
	'
	<table class="table table-bordered table-striped table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>Disease Name</th>
				'.$items_list.'
				<th>Total</th>
			</tr>
		</thead>
		  <tbody>
		  
	';
	//get all administrators
	$personnel_query = $this->personnel_model->get_all_personnel();
	
	
	foreach ($query->result() as $row)
	{
		$diseases_name = $row->diseases_name;
		$diseases_id = $row->diseases_id;

		$items_lists = '';
		//  get the quantities of diseases detected
		$year_search = $this->session->userdata('moh_year');
		$month_search = $this->session->userdata('moh_month');
		if(!empty($year_search) AND !empty($month_search))
		{
			$date = $year_search.'-'.$month_search.'-01';
			$end = $year_search.'-'.$month_search.'-' . date('t', strtotime($date));
		}
		else if(!empty($year_search) AND empty($month_search))
		{
			// $date = $year_search.'-01-01';
			// $end = $year_search.'-12-' . date('t', strtotime($date));
			$date = $year_search.'-'.date('m-').'01';
			$end = $year_search.'-'.date('m').'-' . date('t', strtotime($date)); //get end date of month
		}
		else if(empty($year_search) AND !empty($month_search))
		{
			// $date = $year_search.'-01-01';
			// $end = $year_search.'-12-' . date('t', strtotime($date));
			$date = date('Y-').$month_search.'-01';
			$end = date('Y-').$month_search.'-' . date('t', strtotime($date)); //get end date of month
		}
		else
		{
			$date = date('Y-m-').'01';
			$end = date('Y').'-'.date('m').'-' . date('t', strtotime($date)); //get end date of month
		}

		// var_dump($end); die();
		$total_row = 0;
		while(strtotime($date) <= strtotime($end)) {
			$day_num = date('d', strtotime($date));
			$day_name = date('l', strtotime($date));
				
			$counters = $this->moh_reports_model->get_counters_per_disease($diseases_id,$date);
			$total_row += $counters;
			// if($date == "2017-03-10")
			// {
			// 		var_dump($counters); die();
			// }
			$items_lists .= "<td>".$counters."</td>";

			$date = date("Y-m-d", strtotime("+1 day", strtotime($date)));


		}
		// end of getting the diseases detected

		$count++;
		$result .= 
		'
			<tr>
				<td>'.$count.'</td>
				<td>'.$diseases_name.'</td>
				'.$items_lists.'
				<td>'.$total_row.'</td>
			</tr> 
		';
	}
	
	$result .= 
	'
	  </tbody>
	</table>
	';
}

else
{
	$result .= "There are no diagnosis";
}
?>

<?php
$title_ext = $title_content;

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $title;?></title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css">
		<script type="text/javascript" src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery/jquery.js"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>tableExport.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>jquery.base64.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>html2canvas.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>libs/sprintf.js"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>jspdf.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>libs/base64.js"></script>
		<style type="text/css">
            .receipt_spacing{letter-spacing:0px; font-size: 12px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            .row .col-md-12 table {
                border:solid #000 !important;
                border-width:1px 0 0 1px !important;
                font-size:10px;
            }
            .row .col-md-12 th, .row .col-md-12 td {
                border:solid #000 !important;
                border-width:0 1px 1px 0 !important;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{max-height:70px; margin:0 auto;}
            .table {margin-bottom: 0;}
			@media print
			{
				#page-break
				{
					page-break-after: always;
					page-break-inside : avoid;
				}
				.print-no-display
				{
					display: none !important;
				}
			}
        </style>
    </head>
    <body class="receipt_spacing" onLoad="window.print();return false;">        
        <!-- Widget content -->
        <div id="excel-export">
        	<table class="table table-condensed">
                <tr>
                    <th><?php echo $title_ext;?></th>
                </tr>
            </table>
        	<?php echo $result;?>
        </div>
		<a href="#" class="print-no-display" onClick ="$('#excel-export').tableExport({type:'excel',escape:'false'});">XLS</a>
     </body>
 </html>
