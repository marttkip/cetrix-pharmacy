<div class="row">
	<div class="col-md-12">   	    
		<?php
			echo "
			<table align='center' class='table table-striped table-hover table-condensed'>
				<tr>
					<th>#</th>
					<th >Services/Items</th>
					<th >Units</th>
					<th >Unit Cost (Ksh)</th>
					<th > Total</th>
					<th></th>
					<th></th>
					<th></th>
				</tr>		
			"; 

			$total= 0; 
			$count = 0;
			

			$num_pages = $total_rows/$per_page;

			if($num_pages < 1)
			{
				$num_pages = 0;
			}
			$num_pages = round($num_pages);

			if($page==0)
			{
				$counted = 0;
			}
			else if($page > 0)
			{
				$counted = $per_page*$page;
			}
			// var_dump($invoice_items->num_rows()); die();
			$sub_total= 0; 
			$personnel_query = $this->personnel_model->retrieve_personnel();
				
			if($invoice_items->num_rows() >0){
				
				$visit_date_day = '';
				foreach ($invoice_items->result() as $value => $key1):
					$v_procedure_id = $key1->visit_charge_id;
					$procedure_id = $key1->service_charge_id;
					$service_name = $key1->service_name;
					$date = $key1->date;
					$time = $key1->time;
					$visit_charge_timestamp = $key1->visit_charge_timestamp;
					$visit_charge_amount = $key1->visit_charge_amount;
					$units = $key1->visit_charge_units;
					$procedure_name = $key1->service_charge_name;
					$service_id = $key1->service_id;
					$provider_id = $key1->provider_id;
				
					$sub_total= $sub_total +($units * $visit_charge_amount);
					$visit_date = date('l d F Y',strtotime($date));
					$visit_time = date('H:i A',strtotime($visit_charge_timestamp));
					
					if($visit_date_day != $visit_date)
					{
						
						$visit_date_day = $visit_date;
					}
					else
					{
						$visit_date_day = '';
					}

					

					$can_change = $this->accounts_model->check_if_can_change();
					if($can_change)
					{
						$buttons = "<td>
										<a class='btn btn-sm btn-primary'  onclick='calculatetotal(".$visit_charge_amount.",".$v_procedure_id.", ".$procedure_id.",".$visit_id.")'><i class='fa fa-pencil'></i></a>
									</td>
									<td>
		                            	<button type='button' class='btn btn-sm btn-default' data-toggle='modal' data-target='#remove_charge".$v_procedure_id."'><i class='fa fa-times'></i></button>
										<div class='modal fade' id='remove_charge".$v_procedure_id."' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
										    <div class='modal-dialog' role='document'>
										        <div class='modal-content'>
										            <div class='modal-header'>
										            	<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
										            	<h4 class='modal-title' id='myModalLabel'>Remove Charge</h4>
										            </div>
										            <div class='modal-body'>
										            	
										            	<p>Charge Name : ".$procedure_name." </p>	
										            	<p>Charge Amount : KSH. ".$visit_charge_amount." </p>											              
										                <div class='form-group'>
										                    <label class='col-md-4 control-label'>Description: </label>
										                    
										                    <div class='col-md-8'>
										                        <textarea class='form-control' id='remove_description".$v_procedure_id."' name='remove_description' ></textarea>
										                    </div>
										                </div>
										                
										                <div class='row'>
										                	<div class='col-md-8 col-md-offset-4'>
										                    	<div class='center-align'>
										                        	<button type='submit' class='btn btn-primary' onclick='remove_charged_item(".$v_procedure_id.",".$visit_id.")'>Remove Charge</button>
										                        </div>
										                    </div>
										                </div>
										            </div>
										            <div class='modal-footer'>
										                <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
										            </div>
										        </div>
										    </div>
										</div>

		                            </td>
									<td>
										<a class='btn btn-sm btn-warning'  onclick='reverse_charged_value(".$v_procedure_id.", ".$visit_id.")'><i class='fa fa-refresh'></i></a>
									</td>

									";
						
					}
					else
					{
						$buttons = '';
					}


					$counted++;
					echo"
							<tr> 
								<td>".$counted."</td>		
								<td >".$procedure_name."</td>
								<td align='center'>
									<input type='text' id='units".$v_procedure_id."' class='form-control' value='".$units."' size='3' />
								</td>
								<td align='center'><input type='text' id='billed_amount".$v_procedure_id."' class='form-control'  size='5' value='".$visit_charge_amount."'></div>
								</td>
								
								<td >".number_format($visit_charge_amount*$units)."</td>
								
								".$buttons."
							</tr>	
					";

					$visit_date_day = $visit_date;
					endforeach;
					

			}		
			echo"
			 </table>
			";
			?>
	</div>
</div>