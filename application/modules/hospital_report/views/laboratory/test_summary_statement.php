<?php echo $this->load->view('statement_search', '', TRUE);?>
<?php
$search_title = $this->session->userdata('laboratory_title_search');
if(!empty($search_title))
{
	$title_ext = $search_title;
}
else
{
	$title_ext = 'Tests Report for '.date('Y-m-d');
}

?>
<div class="row">
    <div class="col-md-12">

        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title"><?php echo $title;?></h2>
            	 <a href="<?php echo site_url();?>medical-reports/print-procedures-report" target="_blank" class="btn btn-sm btn-warning pull-right" style="margin-top:-25px;"> <i class="fa fa-print"></i> Print List</a>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
          <h5 class="center-align"><?php echo $title_ext;?></h5>
          <br>
<?php
		$result = '';
		$search = $this->session->userdata('laboratory_report_search');
		if(!empty($search))
		{
			echo '<a href="'.site_url().'close-tests-search" class="btn btn-sm btn-warning">Close Search</a>';
		}
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
				'
					<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Visit Date</th>
						  <th>Patient Name</th>
						  <th>Category</th>
						  <th>Amount</th>
						  <th>Created By</th>
						  <th>Billed By </th>
						  </tr>
					  </thead>
					  <tbody>
						  
				';
				
		
			
			// $personnel_query = $this->personnel_model->get_all_personnel();
			
			foreach ($query->result() as $row)
			{
				$total_invoiced = 0;
				$visit_date = date('jS M Y',strtotime($row->visit_date));
				
				$visit_id = $row->visit_id;
				$patient_number = $row->patient_number;
				$personnel_id = $row->personnel_id;
				$visit_type_id = $row->visit_type_id;
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
				$visit_type_name = $row->visit_type_name;
				$visit_charge_amount = $row->visit_charge_amount;


				
				
				$initiated_by = $row->initiated_by;
				$charged_by = $row->charged_by;

				$created_by = $this->laboratory_model->get_personnel_name($initiated_by);
				$charged_by = $this->laboratory_model->get_personnel_name($charged_by);

				$count++;
				
				
					$result .= 
						'
							<tr>
								<td>'.$count.'</td>
								<td>'.$visit_date.'</td>
								<td>'.$patient_surname.' '.$patient_othernames.'</td>
								<td>'.$visit_type_name.'</td>
								<td>'.number_format($visit_charge_amount,2).'</td>	
								<td>'.$created_by.'</td>								
								<td>'.$charged_by.'</td>

						';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no visit procedures done";
		}
		
		echo $result;
?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        
		</section>
    </div>
  </div>