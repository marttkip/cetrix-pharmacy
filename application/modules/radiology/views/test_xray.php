<?php

if(!empty($service_charge_id))
{
	$rs = $this->xray_model->get_xray_visit($visit_id, $service_charge_id);
	$num_visit = count($rs);

	// var_dump($service_charge_id); die();
	$this->xray_model->save_xray_visit($visit_id, $service_charge_id);
}
$rs = $this->xray_model->get_xray_visit2($visit_id);
$num_rows = count($rs);

echo "

<table class='table table-striped table-hover table-condensed'>
	<tr>
		<th>No.</th>
    	<th>Date Done</th>
		<th>Time</th>
    	<th>Service</th>
		<th>Cost</th>
		<th></th>
	</tr>
	<tbody>
";

$total = 0;
$s=0;
foreach ($rs as $key6):
	
	$visit_charge_id = $key6->visit_charge_id;
	$test = $key6->service_charge_name;
	$price = $key6->service_charge_amount;
	$service_charge_id = $key6->service_charge_id;

	$date_done = $key6->date;
	$time = $key6->time;

	$visit_date = date('l d F Y',strtotime($date_done));
	$visit_time = date('H:i A',strtotime($time));
	$total = $total + $price;
	$s++;


	echo "
		<tr>
        	<td>".($s)."</td>
        	<td>".$visit_date."</td>
        	<td>".$visit_time."</td>
			<td>".$test."</td>
			<td>".number_format($price, 2)."</td>
			<td>
				<a class='btn btn-danger btn-sm' href='#' onclick='delete_xray_cost(".$visit_charge_id.", ".$visit_id.");'><i class='fa fa-trash'></i></a>
			</td>
			
		</tr>
	";

endforeach;
$xray_visit = 0;
echo "
	<tr bgcolor='#D9EDF7'>
		<th colspan='4'>Total</th>
		<th>".number_format($total, 2)."</th>
	</tr>";


echo "
	</tbody>
</table>

";

?>