<!-- end #header -->
<section class="panel">
    <header class="panel-heading">
        <div class="panel-title">
        	<?php echo $title;?>
		</div>
		<div class="pull-right">
			
		</div>
    </header>

    <div class="panel-body">
		
		
<div class="row">
  <div class="col-md-12">
  		<?php
  		
  			$personnel_id = $this->session->userdata('personnel_id');
			$is_admin = $this->reception_model->check_if_admin($personnel_id,1);


			$is_lab = $this->reception_model->check_if_admin($personnel_id,25);

			$is_accountant = $this->reception_model->check_if_admin($personnel_id,28);

			if(($is_admin OR $personnel_id == 0) OR $is_lab)
			{
				$visit_id_search = $this->session->userdata('visit_id_lab');
		  		// var_dump($visit_id_search); die();
		  		if($visit_id_search)
		  		{
  				?>
  				<section class="panel panel-featured panel-featured-info">
		            <header class="panel-heading">
		                <h2 class="panel-title">Laboratory</h2>
		            </header>
		            <div class="panel-body">
		                <div class="col-lg-8 col-md-8 col-sm-8">
						  <div class="form-group">
							<select id='lab_test_id' name='lab_test_id' class='form-control custom-select '>
							  <option value=''>None - Please Select a Lab test</option>
							  <?php echo $lab_tests;?>
							</select>
						  </div>
						
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4">
						  <div class="form-group">
							  <button type='submit' class="btn btn-sm btn-success"  onclick="parse_lab_test(<?php echo $visit_id;?>);"> Add Lab Test</button>
						  </div>
						</div>

		            </div>
		           	<div id="lab_table"></div>
				    <div id="test_results"></div>
					
                    </div>
		    	</section>
		    <?php
			}
			else
			{

				
				?>
	  			<section class="panel">
	  				<div class="panel-body">
	  					<div class="row">
	  					<?php
	  					$error = $this->session->userdata('error_message');
						$success = $this->session->userdata('success_message');
						
						if(!empty($error))
						{
							echo '<div class="alert alert-danger">'.$error.'</div>';
							$this->session->unset_userdata('error_message');
						}
						
						if(!empty($success))
						{
							echo '<div class="alert alert-success">'.$success.'</div>';
							$this->session->unset_userdata('success_message');
						}
	  					?>
						  	<div class="col-md-12 center-align">
	  							<a class='btn btn-sm btn-warning ' data-toggle='modal' data-target='#add_assessment'><i class="fa fa-plus"></i> New Walkin </a>

	  							<div class="modal fade bs-example-modal-lg" id="add_assessment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								    <div class="modal-dialog modal-lg" role="document">
								        <div class="modal-content">
								            <div class="modal-header">
								                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								                <h4 class="modal-title" id="myModalLabel">Add New Walkin</h4>
								            </div>
								            <?php echo form_open("create-walkin-visit1", array("class" => "form-horizontal"));?>
								            <div class="modal-body">
								            	<div class="row">
								                	<div class='col-md-12'>
								                      	<div class="form-group">
															<label class="col-lg-4 control-label">Patient Name: </label>
															<div class="col-lg-8">
																<input type="text" class="form-control" name="patient_name" placeholder="" autocomplete="off">
															</div>
														  
														</div>
														 <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
								                      	<div class="form-group">
															<label class="col-lg-4 control-label"> Patient Gender: </label>
														  
															
															<div class="col-lg-8">
																<select id='parent_service_id' name='gender' class='form-control custom-select ' >
											                      <option value='0'>None - Please gender</option>
											                       <option value='1'>Male</option>
											                       <option value='2'>Female</option>

											                    </select>
															</div>
														</div>				
								                      	<div class="form-group">
															<label class="col-lg-4 control-label">Age: </label>
														  
															<div class="col-lg-8">
																<input type="text" class="form-control" name="age" placeholder="" autocomplete="off" >
															</div>
														</div>
								                    </div>
								                </div>
								            </div>
								            <div class="modal-footer">
								            	<button type="submit" class='btn btn-info btn-sm' type='submit' >Add Patient</button>
								                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								                
								            </div>
								            <?php echo form_close();?>
								        </div>
								    </div>
								</div>
	  						</div>
	  					</div>
	  				</div>
	  			</section>

	  			<?php
			}
		}

		if(($is_admin OR $personnel_id == 0) OR $is_accountant)
		{
			if($accounts_mode == TRUE)
			{
				?>
			    	<section class="panel panel-featured panel-featured-info">
				    	<div class="col-md-12">
							<div class="row">
								<header class="panel-heading">						
									<div id="page_header"></div>
								</header>
							</div>
							<div class="row">
								<div id="patient_bill"></div>
							</div>
						</div>
					</section>
				<?php

			}
			else
			{

				?>
				<section class="panel">
	  				<div class="panel-body">
	  					<div class="row">
						  	<div class="col-md-12 center-align">
	  							<a href="<?php echo site_url();?>lab-walkins" class="btn btn-info btn-md " ><i class="fa fa-plus"></i> REFRESH TO CHECK FOR NEW WALKIN </a>
	  						</div>
	  					</div>
	  				</div>
	  			</section>
				<?php

			}
		
		}
		?>
  			
  		
    </div>
</div>


<div class="modal fade bs-example-modal-lg" id="add_payment_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add to Bill</h4>
            </div>
             <?php echo form_open("accounts/add_accounts_personnel", array("class" => "form-horizontal","id"=>"add_payment"));?>
            <div class="modal-body">
            	<div class="row">
            		<div class="col-md-2 ">
            		</div>
		            	<div class="col-md-10 ">
		            		<div class="form-group" style="display: none;">
								<div class="col-lg-4">
                                	<div class="radio">
                                        <label>
                                            <input id="optionsRadios2" type="radio" name="type_payment" id="type_payment" value="1" checked="checked" onclick="getservices(1)"> 
                                            Normal
                                        </label>
                                    </div>
								</div>
								<div class="col-lg-4" style="display: none;">
                                	<div class="radio">
                                        <label>
                                            <input id="optionsRadios2" type="radio" name="type_payment" id="type_payment" value="2" onclick="getservices(2)"> 
                                            Debit Note
                                        </label>
                                    </div>
								</div>
								<div class="col-lg-4" style="display: none;">
                                	<div class="radio">
                                        <label>
                                            <input id="optionsRadios2" type="radio" name="type_payment" id="type_payment" value="3" onclick="getservices(3)"> 
                                            Credit Note
                                        </label>
                                    </div>
								</div>
							</div>
							 
                           	<input type="hidden" name="type_payment" value="1">
                           	<input type="hidden" name="service_id" id="service_id" value="0">
							
                            

							<div class="form-group">
								<label class="col-lg-2 control-label">Amount: </label>
							  
								<div class="col-lg-8">
									<input type="text" class="form-control" name="amount_paid" id="amount_paid" placeholder="" autocomplete="off"  onkeyup="get_change()">
								</div>
							</div>
							<input type="hidden" class="form-control" name="change_payment" id="change_payment" placeholder="" autocomplete="off" >
							
							<div class="form-group" >
								<label class="col-lg-2 control-label">Payment Method: </label>
								  
								<div class="col-lg-8">
									<select class="form-control" name="payment_method" id="payment_method" onchange="check_payment_type(this.value)">
										<option value="0">Select a group</option>
                                    	<?php
										  $method_rs = $this->accounts_model->get_payment_methods();
										  $num_rows = count($method_rs);
										 if($num_rows > 0)
										  {
											
											foreach($method_rs as $res)
											{
											  $payment_method_id = $res->payment_method_id;
											  $payment_method = $res->payment_method;
											  
												echo '<option value="'.$payment_method_id.'">'.$payment_method.'</option>';
											  
											}
										  }
									  ?>
									</select>
								  </div>
							</div>
							<div id="mpesa_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Mpesa TX Code: </label>

								<div class="col-lg-8">
									<input type="text" class="form-control" name="mpesa_code" id="mpesa_code" placeholder="">
								</div>
							</div>
						  
							<div id="insuarance_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Credit Card Detail: </label>
								<div class="col-lg-8">
									<input type="text" class="form-control" name="insuarance_number" id="insuarance_number" placeholder="">
								</div>
							</div>
						  
							<div id="cheque_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Cheque Number: </label>
							  
								<div class="col-lg-8">
									<input type="text" class="form-control" name="cheque_number" id="cheque_number" placeholder="">
								</div>
							</div>
							<div id="bank_deposit_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Deposit Detail: </label>
							  
								<div class="col-lg-8">
									<input type="text" class="form-control" name="deposit_detail" id="deposit_detail" placeholder="">
								</div>
							</div>
							<div id="debit_card_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Debit Card Detail: </label>
							  
								<div class="col-lg-8">
									<input type="text" class="form-control" name="debit_card_detail" id="debit_card_detail" placeholder="">
								</div>
							</div>
						  
							<div id="username_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Username: </label>
							  
								<div class="col-lg-8">
									<input type="text" class="form-control" name="username" id="username" placeholder="">
								</div>
							</div>
						  
							<div id="password_div" class="form-group" style="display:none;" >
								<label class="col-lg-2 control-label"> Password: </label>
							  
								<div class="col-lg-8">
									<input type="password" class="form-control" name="password" id="password" placeholder="">
								</div>
							</div>
			            </div>
			           <input type="hidden" name="visit_id_payments" id="visit_id_payments">
			        </div>
            </div>
            <div class="modal-footer">
            	<h4 class="pull-left" > Change : <span id="change_item"></span></h4>
            	<button type="submit" class='btn btn-info btn-sm' type='submit' >Add Payment</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            <?php echo form_close();?>
        </div>
    </div>
</div>
  
 </div>
 </section>

  
<script type="text/javascript">
	$(document).ready(function(){
	  display_inpatient_prescription(<?php echo $visit_id;?>,1);
	  display_patient_bill(<?php echo $visit_id;?>);
	  // document.getElementById("visit_id_checked").value = <?php echo $visit_id;?>;
	  document.getElementById("visit_id_payments").value = <?php echo $visit_id;?>;
	  // document.getElementById("visit_id_visit").value = <?php echo $visit_id;?>;
	});

	function display_patient_bill(visit_id){

		var XMLHttpRequestObject = false;
		  
		if (window.XMLHttpRequest) {

		  XMLHttpRequestObject = new XMLHttpRequest();
		} 
		  
		else if (window.ActiveXObject) {
		  XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		}

		var config_url = document.getElementById("config_url").value;
		var url = config_url+"walkins/view_patient_bill/"+visit_id;
		// alert(url);
		if(XMLHttpRequestObject) {
		          
		  XMLHttpRequestObject.open("GET", url);
		          
		  XMLHttpRequestObject.onreadystatechange = function(){
		      
		      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

		        document.getElementById("patient_bill").innerHTML=XMLHttpRequestObject.responseText;
		        get_patient_receipt(visit_id);
				get_page_header(visit_id);
		      }
		  }   
		  XMLHttpRequestObject.send(null);
		}
      
  	}
  	function get_patient_receipt(visit_id){

      var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"walkins/get_patient_receipt/"+visit_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("payments-made").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
      get_page_header(visit_id);
  }



  	function get_page_header(visit_id)
	{
		// alert()
		 var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"walkins/get_patient_details_header/"+visit_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("page_header").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }

	}



	function get_change()
	{

		var visit_id = $('#visit_id_payments').val();
	
		var amount_paid = $('#amount_paid').val();
		var url = "<?php echo base_url();?>accounts/get_change/"+visit_id;
	
		$.ajax({
		type:'POST',
		url: url,
		data:{visit_id: visit_id, amount_paid: amount_paid},
		dataType: 'json',
		success:function(data){
			var change = data.change;

			document.getElementById("change_payment").value = change;
			$('#change_item').html("Kes."+data.change);

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			display_patient_bill(visit_id);
		}
		});
		return false;
	}
	function check_payment_type(payment_type_id){
   
    var myTarget1 = document.getElementById("cheque_div");

    var myTarget2 = document.getElementById("mpesa_div");

    var myTarget4 = document.getElementById("debit_card_div");

    var myTarget5 = document.getElementById("bank_deposit_div");

    var myTarget3 = document.getElementById("insuarance_div");

    if(payment_type_id == 1)
    {
      // this is a check     
      myTarget1.style.display = 'block';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
      myTarget4.style.display = 'none';
      myTarget5.style.display = 'none';
    }
    else if(payment_type_id == 2)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
      myTarget4.style.display = 'none';
      myTarget5.style.display = 'none';
    }
    else if(payment_type_id == 7)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
      myTarget4.style.display = 'none';
      myTarget5.style.display = 'block';
    }
    else if(payment_type_id == 8)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
      myTarget4.style.display = 'block';
      myTarget5.style.display = 'none';
    }
    else if(payment_type_id == 5)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'block';
      myTarget3.style.display = 'none';
      myTarget4.style.display = 'none';
      myTarget5.style.display = 'none';
    }
    else if(payment_type_id == 6)
    {
       myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';
      myTarget4.style.display = 'none';
      myTarget5.style.display = 'none';  
    }

  }

	function get_next_invoice_page(page,visit_id)
	{
		// alert(page);
		 var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"walkins/view_patient_bill/"+visit_id+"/"+page;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("patient_bill").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }

	}

	function get_next_payments_page(page,visit_id)
	{
		// alert(page);
		 var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"walkins/get_patient_receipt/"+visit_id+"/"+page;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("payments-made").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }

	}

	//Calculate procedure total
	function calculatetotal(amount, id, procedure_id, v_id){
	       
	    var units = document.getElementById('units'+id).value;  
	    var billed_amount = document.getElementById('billed_amount'+id).value;  

	    grand_total(id, units, billed_amount, v_id);
	}
	function grand_total(procedure_id, units, amount, v_id){
    	var config_url = document.getElementById("config_url").value;
    	var url = config_url+"accounts/update_service_total/"+procedure_id+"/"+units+"/"+amount+"/"+v_id;
	
		$.ajax({
		type:'POST',
		url: url,
		data:{visit_id: v_id},
		dataType: 'json',
		success:function(data){
			alert(data.message);
			display_patient_bill(v_id);
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			display_patient_bill(v_id);
		}
		});
		return false;

	    
	   
	}

	$(document).on("submit","form#add_payment",function(e)
	{
		e.preventDefault();	

		var payment_method = $('#payment_method').val();
		var amount_paid = $('#amount_paid').val();
		var type_payment =  $("input[name='type_payment']:checked").val(); //$('#type_payment').val();
		var service_id = $('#service_id').val();
		var cheque_number = $('#cheque_number').val();
		var insuarance_number = $('#insuarance_number').val();
		var mpesa_code = $('#mpesa_code').val();
		var username = $('#username').val();
		var password = $('#password').val();
		var change_payment = $('#change_payment').val();

		var debit_card_detail = $('#debit_card_detail').val();
		var deposit_detail = $('#deposit_detail').val();
		var password = $('#password').val();


		var visit_id = $('#visit_id_payments').val();

		var payment_service_id = $('#payment_service_id').val();

	
		var url = "<?php echo base_url();?>accounts/make_payments/"+visit_id;
		// alert(type_payment);
		$.ajax({
		type:'POST',
		url: url,
		data:{payment_method: payment_method, amount_paid: amount_paid, type_payment: type_payment,service_id: service_id, cheque_number: cheque_number, insuarance_number: insuarance_number, mpesa_code: mpesa_code,username: username,password: password, payment_service_id: payment_service_id,debit_card_detail: debit_card_detail,deposit_detail: deposit_detail,change_payment:change_payment},
		dataType: 'json',
		success:function(data){

			if(data.result == 'success')
        	{
				alert(data.message);

			 	$('#add_payment_modal').modal('toggle');
			 	get_page_header(visit_id);
      			get_patient_receipt(visit_id,null);
				 
			}
			else
			{
				alert(data.message);
			}
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			display_patient_bill(visit_id);
		}
		});
		return false;
	});
	function delete_service(id, visit_id){

		var res = confirm('Are you sure you want to delete this charge?');
     
	    if(res)
	    {

	    	var config_url = document.getElementById("config_url").value;
	    	var url = config_url+"accounts/delete_service_billed/"+id+"/"+visit_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{visit_id: visit_id,id: id},
			dataType: 'json',
			success:function(data){
				alert(data.message);
				display_patient_bill(visit_id);
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				display_patient_bill(visit_id);
			}
			});
			return false;
		    var XMLHttpRequestObject = false;
		        
		    if (window.XMLHttpRequest) {
		    
		        XMLHttpRequestObject = new XMLHttpRequest();
		    } 
		        
		    else if (window.ActiveXObject) {
		        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		    }
		     var config_url = document.getElementById("config_url").value;
		    var url = config_url+"accounts/delete_service_billed/"+id;
		    
		    if(XMLHttpRequestObject) {
		                
		        XMLHttpRequestObject.open("GET", url);
		                
		        XMLHttpRequestObject.onreadystatechange = function(){
		            
		            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

		                display_patient_bill(visit_id);
		            }
		        }
		                
		        XMLHttpRequestObject.send(null);
		    }
		}
	}

	$(document).on("submit","form#payments-paid-form",function(e)
	{
		// alert("changed");
		e.preventDefault();	

		
		var visit_id = $('#visit_id').val();
		var payment_id = $('#payment_id').val();
		var cancel_action_id = $('#cancel_action_id').val();
		var cancel_description = $('#cancel_description').val();
		var url = "<?php echo base_url();?>accounts/cancel_payment/"+payment_id+"/"+visit_id;	

		
		// alert(cancel_action_id);	
		$.ajax({
		type:'POST',
		url: url,
		data:{cancel_description: cancel_description, cancel_action_id: cancel_action_id},
		dataType: 'text',
		success:function(data){
			alert("You have successfully cancelled a payment");
		 	$('#refund_payment'+visit_id).modal('toggle');
		 	get_page_header(visit_id);
			get_patient_receipt(visit_id);
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			get_page_header(visit_id);
			get_patient_receipt(visit_id);
		}
		});
		return false;
	});

	$(function() {
	    $("#drug_id").customselect();
	     $("#service_id_item").customselect();
       	$("#provider_id_item").customselect();

	  });
	
	function myPopup2(visit_id,module) {
		var config_url = $('#config_url').val();
		var win_drugs = window.open(config_url+"pharmacy/drugs/"+visit_id+"/"+module,"Popup3","height=1200,width=1000,,scrollbars=yes,"+ 
							"directories=yes,location=yes,menubar=yes," + 
							 "resizable=no status=no,history=no top = 50 left = 100"); 
  		win_drugs.focus();
	}
	
	function myPopup2_soap(visit_id) {
		var config_url = $('#config_url').val();
		var win_drugs = window.open(config_url+"pharmacy/drugs/"+visit_id,"Popup2","height=1200,width=1000,,scrollbars=yes,"+ 
							"directories=yes,location=yes,menubar=yes," + 
							 "resizable=no status=no,history=no top = 50 left = 100"); 
  		win_drugs.focus();
	}
	
	function send_to_pharmacy2(visit_id)
	{
		var config_url = $('#config_url').val();
		var url = config_url+"pharmacy/display_prescription/"+visit_id;
	
		$.get(url, function( data ) {
			var obj = window.opener.document.getElementById("prescription");
			obj.innerHTML = data;
			window.close(this);
		});
	}
</script>





<script type="text/javascript">
  	$(function() {
	    $("#lab_test_id").customselect();
	    $("#consumable_id").customselect();
	});
	  $(document).ready(function(){
	       get_test_results(100, <?php echo $visit_id?>);
		   get_lab_table(<?php echo $visit_id;?>);
		   get_visit_results(<?php echo $visit_id?>);
			display_visit_consumables(<?php echo $visit_id;?>);
			$(function() {
				$("#lab_test_id").customselect();
			});
	  });
	   function parse_lab_test(visit_id)
	  {
	    var lab_test_id = document.getElementById("lab_test_id").value;
	     lab(lab_test_id, visit_id);
	    
	  }
	  function lab(id, visit_id){
    
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var url = "<?php echo site_url();?>laboratory/test_lab/"+visit_id+"/"+id;
	    // window.alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	                
	               document.getElementById("lab_table").innerHTML = XMLHttpRequestObject.responseText;
	               get_lab_table(visit_id);
	               get_test_results(100, visit_id);
	            }
	        }
	        
	        XMLHttpRequestObject.send(null);
	    }
	}
	function get_lab_table(visit_id){
        var XMLHttpRequestObject = false;
            
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var url = "<?php echo site_url();?>laboratory/confirm_lab_test_charge/"+visit_id;
		
        if(XMLHttpRequestObject) {
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                    
                    document.getElementById("lab_table").innerHTML = XMLHttpRequestObject.responseText;
                      get_test_results(100, visit_id);

                }
            }
            
            XMLHttpRequestObject.send(null);
        }
    }
  	function open_window_lab(test, visit_id){
	  var config_url = $('#config_url').val();
	  window.open(config_url+"laboratory/laboratory_list/"+test+"/"+visit_id,"Popup","height=1200, width=800, , scrollbars=yes, "+ "directories=yes,location=yes,menubar=yes," + "resizable=no status=no,history=no top = 50 left = 100");
	}
	function get_test_results(page, visit_id){

	  var XMLHttpRequestObject = false;
	    
	  if (window.XMLHttpRequest) {
	  
	    XMLHttpRequestObject = new XMLHttpRequest();
	  } 
	    
	  else if (window.ActiveXObject) {
	    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	  }
	  var config_url = $('#config_url').val();
	  if((page == 1) || (page == 65) || (page == 85)){
	    
	    url = config_url+"laboratory/test/"+visit_id;
	  }
	  
	  else if ((page == 75) || (page == 100)){
	    url = config_url+"laboratory/test1/"+visit_id;
	  }
		// alert(url);
	  if(XMLHttpRequestObject) {
	    if((page == 75) || (page == 85)){
	      var obj = window.opener.document.getElementById("test_results");
	    }
	    else{
	      var obj = document.getElementById("test_results");
	    }


	    var config_url = $('#config_url').val();
		
		var data_url = config_url+"laboratory/check_payment_status/"+visit_id;
         	// alert(data_url);
        $.ajax({
			type:'POST',
			url: data_url,
			data:{visit_id: visit_id},
			dataType: 'text',
			success:function(data){
				//$("#result_space"+format).val(data);
				 var data = jQuery.parseJSON(data);
				 var balance = data.balance;
				 // alert(balance);
				 // if(balance > 0)
				 // {
				 // 	$('#test_results').css('display', 'none');
				 // }
				 // else
				 // {
				 	$('#test_results').css('display', 'block');
				 // }
			},
			error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}
        });

        
	    XMLHttpRequestObject.open("GET", url);
	    
	    XMLHttpRequestObject.onreadystatechange = function(){
	    
	      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	  		//window.alert(XMLHttpRequestObject.responseText);
	        obj.innerHTML = XMLHttpRequestObject.responseText;
	        if((page == 75) || (page == 85)){
	          window.close(this);
	        }
	        
	      }
	    }
	    XMLHttpRequestObject.send(null);
	  }
	}


	function get_visit_results(visit_id){

	  var XMLHttpRequestObject = false;
	    
	  if (window.XMLHttpRequest) {
	  
	    XMLHttpRequestObject = new XMLHttpRequest();
	  } 
	    
	  else if (window.ActiveXObject) {
	    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	  }
	  var config_url = $('#config_url').val();
	  
	   url = config_url+"laboratory/visit_results/"+visit_id;
		// alert(url);
	  if(XMLHttpRequestObject) {
	    
	    var obj = document.getElementById("visit_results");
	    
	    XMLHttpRequestObject.open("GET", url);
	    
	    XMLHttpRequestObject.onreadystatechange = function(){
	    
	      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	  		//window.alert(XMLHttpRequestObject.responseText);
	        obj.innerHTML = XMLHttpRequestObject.responseText;
	        
	      }
	    }
	    XMLHttpRequestObject.send(null);
	  }
	}


	function save_result_format(visit_lab_test_id, lab_test_format_id, visit_id,lab_visit_results_id)
	{
		var config_url = $('#config_url').val();
		
		var res = document.getElementById("laboratory_result2"+lab_visit_results_id).value;
		var data_url = config_url+"laboratory/save_result_lab";
         	// alert(data_url);
        $.ajax({
			type:'POST',
			url: data_url,
			data:{res: res, format: lab_test_format_id, visit_id: visit_id, visit_lab_test_id: visit_lab_test_id,lab_visit_results_id: lab_visit_results_id},
			dataType: 'text',
			success:function(data){
				//$("#result_space"+format).val(data);
			},
			error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}
        });
	}

	function save_lab_comment(id, visit_id)
	{
		var config_url = $('#config_url').val();
		
		var res = document.getElementById("laboratory_comment"+id).value;
		
		var data_url = config_url+"laboratory/save_lab_comment";
			
		$.ajax({
			type:'POST',
			url: data_url,
			data:{visit_charge_id: id, lab_visit_format_comments: res, visit_id: visit_id},
			dataType: 'text',
			success:function(data){
				//$("#result_space"+format).val(data);
			},
			error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}
	
		});
	}

	function save_result(id, visit_id){
		
		var config_url = $('#config_url').val();
		var res = document.getElementById("laboratory_result"+id).value;
        var data_url = config_url+"laboratory/save_result/"+id+"/"+res+"/"+visit_id;
   	 // window.alert(data_url);
         var result_space = $('#result_space'+id).val();//document.getElementById("vital"+vital_id).value;
         	
        $.ajax({
			type:'POST',
			url: data_url,
			data:{result: result_space},
			dataType: 'text',
			success:function(data){
				//$("#result_space"+id).val(data);
			},
			error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}

        });
	
		
	}
	function send_to_doc(visit_id){
	

		var XMLHttpRequestObject = false;
			
		if (window.XMLHttpRequest) {
		
			XMLHttpRequestObject = new XMLHttpRequest();
		} 
			
		else if (window.ActiveXObject) {
			XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var config_url = $('#config_url').val();

		var url = config_url+"laboratory/send_to_doctor/"+visit_id;
					
		if(XMLHttpRequestObject) {
					
			XMLHttpRequestObject.open("GET", url);
					
			XMLHttpRequestObject.onreadystatechange = function(){
				
				if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
					
					//window.location.href = host+"index.php/laboratory/lab_queue";
				}
			}
					
			XMLHttpRequestObject.send(null);
		}
	}
	function finish_lab_test(visit_id){

		var XMLHttpRequestObject = false;
			
		if (window.XMLHttpRequest) {
		
			XMLHttpRequestObject = new XMLHttpRequest();
		} 
			
		else if (window.ActiveXObject) {
			XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var config_url = $('#config_url').val();
		var url = config_url+"laboratory/finish_lab_test/"+visit_id;
				
		if(XMLHttpRequestObject) {
					
			XMLHttpRequestObject.open("GET", url);
					
			XMLHttpRequestObject.onreadystatechange = function(){
				
				if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
					
					//window.location.href = host+"index.php/laboratory/lab_queue";
				}
			}
					
			XMLHttpRequestObject.send(null);
		}
	}

	function save_comment(visit_charge_id){
		var config_url = $('#config_url').val();
		var comment = document.getElementById("test_comment").value;
        var data_url = config_url+"laboratory/save_comment/"+comment+"/"+visit_charge_id;
     
        // var comment_tab = $('#comment').val();//document.getElementById("vital"+vital_id).value;
         	
        $.ajax({
        type:'POST',
        url: data_url,
       // data:{comment: comment_tab},
        dataType: 'text',
        success:function(data){
        //obj.innerHTML = XMLHttpRequestObject.responseText;
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
       // alert(error);
        }

        });
	

		
	}
	function update_lab_test_charge(visit_lab_test_id,visit_id){
		var config_url = $('#config_url').val();
		var lab_test_amount = document.getElementById("lab_test_price"+visit_lab_test_id).value;
		var charge_date = document.getElementById("charge_date"+visit_lab_test_id).value;
        var data_url = "<?php echo site_url();?>laboratory/update_lab_charge_amount/"+visit_lab_test_id+"/"+visit_id;

		  $.ajax({
		  type:'POST',
		  url: data_url,
		  data:{amount: lab_test_amount,charge_date: charge_date},
		  dataType: 'text',
		  success:function(data){
		    window.alert("You have successfully updated the lab test amount");
		    
		   get_lab_table(visit_id);
		  //obj.innerHTML = XMLHttpRequestObject.responseText;
		  },
		  error: function(xhr, status, error) {
		  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		  alert(error);
		  }

		  });
		   get_lab_table(visit_id);
		
	}

	function print_previous_test(visit_id, patient_id){
		var config_url = $('#config_url').val();
    	window.open(config_url+"laboratory/print_test/"+visit_id+"/"+patient_id,"Popup","height=900,width=1200,,scrollbars=yes,"+
                        "directories=yes,location=yes,menubar=yes," +
                         "resizable=no status=no,history=no top = 50 left = 100");
	}
	function parse_consumable_charge(visit_id,suck)
    {
      var consumable_id = document.getElementById("consumable_id").value;
       // alert(consumable_id);
      consumable(consumable_id, visit_id,suck);

    }

    function consumable(id, visit_id,suck){
        
        var XMLHttpRequestObject = false;
            
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var url = "<?php echo site_url();?>nurse/inpatient_consumables/"+id+"/"+visit_id+"/"+suck;
        
        if(XMLHttpRequestObject) {
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                    
                   document.getElementById("consumables_to_patients").innerHTML = XMLHttpRequestObject.responseText;
                   //get_surgery_table(visit_id);
                }
            }
            
            XMLHttpRequestObject.send(null);
        }
    }
	function delete_consumable(id, visit_id)
	{
		var confirmation = confirm('Delete consumable?');
	
		if(confirmation)
		{
			var XMLHttpRequestObject = false;
				
			if (window.XMLHttpRequest) {
			
				XMLHttpRequestObject = new XMLHttpRequest();
			} 
				
			else if (window.ActiveXObject) {
				XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
			}
			var config_url = document.getElementById("config_url").value;
			var url = config_url+"nurse/delete_consumable/"+id+"/"+visit_id;;
			
			if(XMLHttpRequestObject) {
						
				XMLHttpRequestObject.open("GET", url);
						
				XMLHttpRequestObject.onreadystatechange = function(){
					
					if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
					{
	            		document.getElementById("consumables_to_patients").innerHTML = XMLHttpRequestObject.responseText;
					}
				}
						
				XMLHttpRequestObject.send(null);
			}
		}
	}
	function display_visit_consumables(visit_id)
	{
		var config_url = document.getElementById("config_url").value;
		var url = config_url+"nurse/visit_consumables/"+visit_id;
		$.get(url, function( data ) 
		{
			$("#consumables_to_patients").html(data);
		});
	}
	function calculateconsumabletotal(amount, id, procedure_id, v_id)
	{
		var units = document.getElementById('units'+id).value;  
		//alert(units);
		grand_consumable_total(id, units, amount, v_id);
	}
	function grand_consumable_total(vaccine_id, units, amount, v_id)
	{
		var XMLHttpRequestObject = false;
			
		if (window.XMLHttpRequest) {
		
			XMLHttpRequestObject = new XMLHttpRequest();
		} 
			
		else if (window.ActiveXObject) {
			XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var config_url = document.getElementById("config_url").value;
		
		var url = config_url+"nurse/consuamble_total/"+vaccine_id+"/"+units+"/"+amount;
		//alert(vaccine_id);
		if(XMLHttpRequestObject) {
					
			XMLHttpRequestObject.open("GET", url);
					
			XMLHttpRequestObject.onreadystatechange = function(){
				
				if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
					display_visit_consumables(v_id);
				}
			}
					
			XMLHttpRequestObject.send(null);
		}
	}

  </script>