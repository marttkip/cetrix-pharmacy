<div class="row">
	<div class="col-md-12">
		
		<section class="panel">
			<header class="panel-heading">
				<h2 class="panel-title">Add Prescription</h2>
			</header>

			<div class="panel-body">
				<?php echo "
				<div class='center-align'>
					<input name='close' type='button' value='Close' class='btn btn-primary' onclick='close_window(".$visit_id.")' />
				</div>";
				?>
                <div class="row" style="padding:30px 0 30px 0;">
                    <div class="col-xs-8">
                        <select id='drug_id' name='drug_id' class="form-control custom-select">
                            <option value=''>None - Please Select an drug</option>
                            <?php echo $drugs;?>
                        </select>
                    </div>
                    <div class="col-xs-4">
                        <button type='submit' class="btn btn-sm btn-success"  onclick="get_drug_to_prescribe(<?php echo $visit_id;?>);"> Prescribe drug</button>
                    </div>
                </div>
                
             	<div id="prescription_view"></div>
             	<!--<div id="visit_prescription"></div>-->
				<?php echo "
				<div class='center-align'>
					<input name='close' type='button' value='Close' class='btn btn-primary' onclick='close_window(".$visit_id.")' />
				</div>";
				?>   
            </div>
        </section>
    </div>
</div>
<?php echo form_close();?>

<script type="text/javascript">
$(document).ready(function(){

    $("#drug_id").customselect();
});
function close_window(visit_id)
{
	window.close(this);
}
function get_drug_to_prescribe(visit_id)
{
  var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var drug_id = document.getElementById("drug_id").value;

    var url = "<?php echo site_url();?>pharmacy/inpatient_prescription/"+visit_id+"/"+drug_id+"/2";

     if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
              var prescription_view = document.getElementById("prescription_view");
             
              document.getElementById("prescription_view").innerHTML=XMLHttpRequestObject.responseText;
               prescription_view.style.display = 'block';
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function pass_prescription()
{
  var quantity = document.getElementById("quantity_value").value;
  var x = document.getElementById("x_value").value;
  // var duration = document.getElementById("duration_value").value;
  var consumption = document.getElementById("consumption_value").value;
  var number_of_days = document.getElementById("number_of_days_value").value;
  var service_charge_id = document.getElementById("drug_id").value;
  var visit_id = document.getElementById("visit_id").value;
  var module = document.getElementById("module").value;
  var passed_value = document.getElementById("passed_value").value;

  var url = "<?php echo base_url();?>pharmacy/prescribe_prescription";


	$.ajax({
		type:'POST',
		url: url,
		data:{quantity: quantity, x: x,consumption: consumption, service_charge_id : service_charge_id, visit_id : visit_id, number_of_days: number_of_days,module: module,passed_value:passed_value},
		dataType: 'text',
		success:function(data){
			alert(data);
			var prescription_view = document.getElementById("prescription_view");
			prescription_view.style.display = 'none';
			display_inpatient_prescription(visit_id,0);
		
		},
		error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		
		}
	});
 
  return false;
}

function display_inpatient_prescription(visit_id,module){

    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"pharmacy/display_inpatient_prescription/"+visit_id+"/"+module;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
			{
				window.opener.document.getElementById("visit_prescription").innerHTML=XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}
</script>