CREATE OR REPLACE VIEW v_product_stock_non_pharm AS 
SELECT
       `product_return_stock`.`product_deductions_stock_id` AS transactionId,
       `product_return_stock`.`product_id` AS `product_id`,
       `product`.`category_id` AS `category_id`,
        product_return_stock.from_store_id AS `store_id`,
        product_return_stock.to_store_id AS `receiving_store`,
        product.product_name AS `product_name`,
        store.store_name AS `store_name`,
        CONCAT('Store Transfer') AS `transactionDescription`,
        '0' AS `dr_quantity`,
        (product_deductions_stock_quantity * product_deductions_stock_pack_size) AS `cr_quantity`,
        '0' AS `dr_amount`,
       (product.product_unitprice* (product_deductions_stock_quantity * product_deductions_stock_pack_size)) AS `cr_amount`,
       `product_return_stock`.`product_deductions_stock_date` AS `transactionDate`,
       `product`.`product_status` AS `status`,
       `product`.`product_deleted` AS `product_deleted`,
       'Expense' AS `transactionCategory`,
       'Product Addition' AS `transactionClassification`,
       'product_return_stock' AS `transactionTable`,
       'product' AS `referenceTable`
FROM (`product_return_stock`,product,store)
WHERE product.product_id = product_return_stock.product_id AND product.product_deleted = 0
AND store.store_id = product_return_stock.from_store_id

UNION ALL

SELECT
       `product_return_stock`.`product_deductions_stock_id` AS transactionId,
       `product_return_stock`.`product_id` AS `product_id`,
       `product`.`category_id` AS `category_id`,
        product_return_stock.to_store_id AS `store_id`,
        product_return_stock.from_store_id AS `receiving_store`,
        product.product_name AS `product_name`,
        store.store_name AS `store_name`,
        CONCAT('Store Transfer') AS `transactionDescription`,
        (product_deductions_stock_quantity * product_deductions_stock_pack_size) AS `dr_quantity`,
        '0' AS `cr_quantity`,
        (product.product_unitprice* (product_deductions_stock_quantity * product_deductions_stock_pack_size)) AS `dr_amount`,
        '0' AS `cr_amount`,
       `product_return_stock`.`product_deductions_stock_date` AS `transactionDate`,
       `product`.`product_status` AS `status`,
       `product`.`product_deleted` AS `product_deleted`,
       'Income' AS `transactionCategory`,
       'Store Deduction' AS `transactionClassification`,
       'product_return_stock' AS `transactionTable`,
       'product' AS `referenceTable`
FROM (`product_return_stock`,product,store)
WHERE product.product_id = product_return_stock.product_id AND product.product_deleted = 0
AND store.store_id = product_return_stock.to_store_id


UNION ALL

-- store deductions
SELECT
  `product_deductions`.`product_deductions_id` AS `transactionId`,
       `product`.`product_id` AS `product_id`,
       `product`.`category_id` AS `category_id`,
       `product_deductions`.`store_id` AS `store_id`,
       `product`.`store_id` AS `receiving_store`,
       product.product_name AS `product_name`,
       store.store_name AS `store_name`,
       CONCAT('Product Added',' ',`product`.`product_name`) AS `transactionDescription`,
        product_deductions.quantity_given AS `dr_quantity`,
   '0' AS `cr_quantity`,
        (product.product_unitprice * product_deductions.quantity_given) AS `dr_amount`,
        '0' AS `cr_amount`,
       `product_deductions`.`search_date` AS `transactionDate`,
       `product`.`product_status` AS `status`,
       `product`.`product_deleted` AS `product_deleted`,
       'Income' AS `transactionCategory`,
       'Product Addition' AS `transactionClassification`,
       'product_deductions' AS `transactionTable`,
       'product' AS `referenceTable`
FROM (`product_deductions`, `store`, `product`, `orders`)
WHERE `product_deductions`.`store_id` = store.store_id
AND product_deductions.quantity_requested > 0
AND product.product_deleted = 0
AND product_deductions.product_id = product.product_id
AND product_deductions.order_id = orders.order_id
AND orders.order_id = product_deductions.order_id
AND (orders.is_store = 1 OR orders.is_store = 0)
AND product_deductions.product_deduction_rejected = 0

UNION ALL
-- Debit Store
SELECT
  `product_deductions`.`product_deductions_id` AS `transactionId`,
       `product`.`product_id` AS `product_id`,
       `product`.`category_id` AS `category_id`,
       product.store_id AS `store_id`,
       product_deductions.store_id AS `receiving_store`,
       product.product_name AS `product_name`,
       store.store_name AS `store_name`,
       CONCAT('Product Deducted',' ',`product`.`product_name`) AS `transactionDescription`,
        '0' AS `dr_quantity`,
     product_deductions.quantity_given AS `cr_quantity`,
        '0' AS `dr_amount`,
        (product.product_unitprice * product_deductions.quantity_given) AS `cr_amount`,
       `product_deductions`.`search_date` AS `transactionDate`,
       `product`.`product_status` AS `status`,
       `product`.`product_deleted` AS `product_deleted`,
       'Expense' AS `transactionCategory`,
       'Store Deduction' AS `transactionClassification`,
       'product_deductions' AS `transactionTable`,
       'product' AS `referenceTable`
FROM (`product_deductions`, `store`, `product`, `orders`)
WHERE store.store_id = product_deductions.store_id
AND product_deductions.quantity_requested > 0
AND product_deductions.product_id = product.product_id
AND product_deductions.order_id = orders.order_id
AND orders.order_id = product_deductions.order_id
AND product.product_deleted = 0
AND (orders.is_store = 1 OR orders.is_store = 0)
AND product_deductions.product_deduction_rejected = 0