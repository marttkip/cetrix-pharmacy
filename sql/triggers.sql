DELIMITER $$
DROP TRIGGER IF EXISTS `t_product_stock_non_pharm_addition` $$
CREATE TRIGGER t_product_stock_non_pharm_addition 
AFTER INSERT ON product_return_stock
AS
BEGIN
	-- SET NOCOUNT ON;

	INSERT INTO t_product_stock_non_pharm (transactionId, product_id,category_id,store_id,receiving_store,product_name,store_name,transactionDescription,dr_quantity
																					,cr_quantity,dr_amount,cr_amount,transactionDate,status,product_deleted,transactionCategory,transactionClassification,
																					transactionTable,referenceTable)
    SELECT
					 `product_return_stock`.`product_deductions_stock_id`,
					 `product_return_stock`.`product_id`,
					 `product`.`category_id`,
						product_return_stock.from_store_id,
						product_return_stock.to_store_id,
						product.product_name,
						store.store_name,
						CONCAT('Store Transfer'),
						'0',
						(product_deductions_stock_quantity * product_deductions_stock_pack_size),
						'0',
					 (product.product_unitprice* (product_deductions_stock_quantity * product_deductions_stock_pack_size)),
					 `product_return_stock`.`product_deductions_stock_date`,
					 `product`.`product_status`,
					 `product`.`product_deleted`,
					 'Expense',
					 'Product Addition',
					 'product_return_stock',
					 'product'
		FROM (`product_return_stock`,product,store)
		WHERE product.product_id = product_return_stock.product_id AND product.product_deleted = 0
		AND store.store_id = product_return_stock.from_store_id

	UNION ALL


	SELECT
		       `product_return_stock`.`product_deductions_stock_id`,
		       `product_return_stock`.`product_id`,
		       `product`.`category_id`,
		        product_return_stock.to_store_id,
		        product_return_stock.from_store_id,
		        product.product_name,
		        store.store_name,
		        CONCAT('Store Transfer'),
		        (product_deductions_stock_quantity * product_deductions_stock_pack_size),
		        '0',
		        (product.product_unitprice* (product_deductions_stock_quantity * product_deductions_stock_pack_size)),
		        '0',
		       `product_return_stock`.`product_deductions_stock_date`,
		       `product`.`product_status`,
		       `product`.`product_deleted`,
		       'Income',
		       'Store Deduction',
		       'product_return_stock',
		       'product'
		FROM (`product_return_stock`,product,store)
		WHERE product.product_id = product_return_stock.product_id AND product.product_deleted = 0
		AND store.store_id = product_return_stock.to_store_id;
END $$
DELIMITER ;